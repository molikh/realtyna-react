import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

const items = [
  {
    id: 1,
    altText: 'Slide 1',
    caption: 'Slide 1'
  },
  {
    id: 2,
    altText: 'Slide 2',
    caption: 'Slide 2'
  },
  {
    id: 3,
    altText: 'Slide 3',
    caption: 'Slide 3'
  }
];

class App extends Component { constructor(props) {
  super(props);
  this.state = { activeIndex: 0 };
  this.next = this.next.bind(this);
  this.previous = this.previous.bind(this);
  this.goToIndex = this.goToIndex.bind(this);
  this.onExiting = this.onExiting.bind(this);
  this.onExited = this.onExited.bind(this);
}

onExiting() {
  this.animating = true;
}

onExited() {
  this.animating = false;
}

next() {
  if (this.animating) return;
  const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
  this.setState({ activeIndex: nextIndex });
}

previous() {
  if (this.animating) return;
  const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
  this.setState({ activeIndex: nextIndex });
}

goToIndex(newIndex) {
  if (this.animating) return;
  this.setState({ activeIndex: newIndex });
}

render() {
  const { activeIndex } = this.state;

  const slides = items.map((item) => {
    return (
      <CarouselItem
        className="custom-tag"
        tag="div"
        key={item.id}
        onExiting={this.onExiting}
        onExited={this.onExited}
      >
    
                  <div className="item">
              <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam doloremque dicta illo ad enim magni fugit fugiat,
                iusto dolor blanditiis maiores repudiandae aliquid eius omnis dignissimos tempora reprehenderit dolores asperiores
                rem aut beatae. Labore fuga eligendi aspernatur! Unde odit exercitationem .</p>
              <button type="button" className="btn btn-light btn-lg btn-block">Button</button>
            </div>
        
      </CarouselItem>
    );
  });

    return (

<div className="container">
  {/* Section One : include card with red bullet header */}
  <div className="row text-center section-one">
    <div className="col-md-3 cards" >
      <div className="card">
        <div className="card-img-top img-circle rounded-circle "></div>

        <div className="card-block ">
          <h4 className="card-title ">Mirion Jones</h4>
          <p className="card-text ">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" className="btn btn-light btn-lg">Button</button>

        </div>
      </div>
    </div>
    <div className="col-md-3 cards" >
      <div className="card">
        <div className="card-img-top img-circle rounded-circle "></div>

        <div className="card-block ">
          <h4 className="card-title ">Mirion Jones</h4>
          <p className="card-text ">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" className="btn btn-light btn-lg">Button</button>

        </div>
      </div>
    </div>
    <div className="col-md-3 cards" >
      <div className="card">
        <div className="card-img-top img-circle rounded-circle "></div>

        <div className="card-block ">
          <h4 className="card-title ">Mirion Jones</h4>
          <p className="card-text ">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" className="btn btn-light btn-lg">Button</button>

        </div>
      </div>
    </div>
    <div className="col-md-3 cards" >
      <div className="card">
        <div className="card-img-top img-circle rounded-circle "></div>

        <div className="card-block ">
          <h4 className="card-title ">Mirion Jones</h4>
          <p className="card-text ">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" className="btn btn-light btn-lg">Button</button>

        </div>
      </div>
    </div>

  </div>

  {/* Section Two : Gallery with slider beside */}
  <div className="section-two">
    <div className="row">
      <div className="col-md-9 gallery">
        <div className="row">
          <div className="col-md-4 gallery-item">
            <div className="card">
              <div className="card-block ">
              </div>
            </div>
          </div>

          <div className="col-md-4 gallery-item">
            <div className="card">
              <div className="card-block ">
                <h4 className="card-title ">Lorem ipsum dolor sit amet.</h4>
                <p className="card-text ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla ab vero soluta, assumenda minima explicabo
                  debitis!
                </p>

              </div>
            </div>
          </div>

          <div className="col-md-4 gallery-item">
            <div className="card">
              <div className="card-block ">
              </div>
            </div>
          </div>

          <div className="col-md-4 gallery-item">
            <div className="card">
              <div className="card-block ">
                <h4 className="card-title ">Lorem ipsum dolor sit amet.</h4>
                <p className="card-text ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla ab vero soluta, assumenda minima explicabo
                  debitis!
                </p>
              </div>
            </div>
          </div>

          <div className="col-md-4 gallery-item">
            <div className="card">

              <div className="card-block ">

              </div>
            </div>
          </div>

          <div className="col-md-4 gallery-item">
            <div className="card">

              <div className="card-block ">
                <h4 className="card-title ">Lorem ipsum dolor sit amet.</h4>
                <p className="card-text ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla ab vero soluta, assumenda minima explicabo
                  debitis!
                </p>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div className="col-md-3 nopadding carousel-container">
      <h3 className="carousel-title ">Lorem, ipsum.</h3>

        <Carousel
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
        >
          <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
          {slides}

        </Carousel>

        {/* <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
          <h3 className="carousel-title ">Lorem, ipsum.</h3>
          <ol className="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active" ></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner">

            <div className="carousel-item active">
              <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam doloremque dicta illo ad enim magni fugit fugiat,
                iusto dolor blanditiis maiores repudiandae aliquid eius omnis dignissimos tempora reprehenderit dolores asperiores
                rem aut beatae. Labore fuga eligendi aspernatur! Unde odit exercitationem .</p>
              <button type="button" className="btn btn-light btn-lg btn-block">Button</button>

            </div>
            <div className="carousel-item ">
              <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam doloremque dicta illo ad enim magni fugit fugiat,
                iusto dolor blanditiis maiores repudiandae aliquid eius omnis dignissimos tempora reprehenderit dolores asperiores
                rem aut beatae. Labore fuga eligendi aspernatur! Unde odit exercitationem .</p>
              <button type="button" className="btn btn-light btn-lg btn-block">Button</button>
            </div>
            <div className="carousel-item ">
              <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam doloremque dicta illo ad enim magni fugit fugiat,
                iusto dolor blanditiis maiores repudiandae aliquid eius omnis dignissimos tempora reprehenderit dolores asperiores
                rem aut beatae. Labore fuga eligendi aspernatur! Unde odit exercitationem .</p>
              <button type="button" className="btn btn-light btn-lg btn-block">Button</button>
            </div>
          </div>

        </div> */}
      </div>
    </div>
  </div>

  <div className="section-three">
    <div className="row">
      {/* Vertical Timeline */}
      <section className="conference-timeline">
        <div className="conference-center-line"></div>
        <div className="conference-timeline-content">
          {/* Article */}
          <div className="timeline-article">
            <div className="content-right-container">
              <div className="content-right">
                <p>When I orbited the Earth in a spaceship, I saw for the first time how beautiful our planet is.
                </p>
              </div>
            </div>
            <div className="meta-date">
              <span className="year">1998</span>
            </div>
          </div>
          {/* // Article */}

          {/* Article */}
          <div className="timeline-article">
            <div className="content-left-container">
              <div className="content-left">
                <p>When I orbited the Earth in a spaceship, I saw for the first time how beautiful our planet is. Mankind, let
                  us preserve and increase this beauty, and not destroy it!
                </p>
              </div>
            </div>

            <div className="meta-date">
              <span className="year">2000</span>
            </div>
          </div>
          {/*  // Article  */}

        </div>
      </section>
      {/* // Vertical Timeline */}
    </div>
  </div>
  </div>

    );
  }
}

export default App;
